__author__ = "Karel Ondřej"

from framework.re import RelationExtractionFramework
from model.re import RelationExtractionModel
from model.attnre import EntityPairRelationExtractionModel
from dataloader.semeval10task8 import SemEval10Task8Dataset
from model.features import FeaturesConverter, MaskedEntityFeaturesConvertor

import json
import sys
import logging
import logging.config


def parser_builder():
    import argparse
    parser = argparse.ArgumentParser(description='Predict relation from Sem Eval 2010 tak 8 datatset.')
    parser.add_argument('--config', default="config.json")
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--save', action='store_true')
    parser.add_argument('--log', default='')
    return parser

def main(args):
    logger = logging.getLogger("train")

    with open(args.config, "r") as f:
        config = json.load(f)

    logger.info("Loading datasets...")
    train_dataset = SemEval10Task8Dataset(config["data_train"])
    test_dataset = SemEval10Task8Dataset(config["data_test"])

    config["label_list"] = train_dataset.labels
    
    logger.info("Model init...")
    if "type" not in config or config["type"] == "RBERT":
        model = RelationExtractionModel(config)
    elif config["type"] == "EntityPairRelationExtractionModel":
        model = EntityPairRelationExtractionModel(**config)
    else:
        logging.error("Unknown model type '" + config["type"] + "'.")
        sys.exit(1)

    config["tokenizer_name"] = config["model_path"]
    features_converter = FeaturesConverter(**config)
    # TODO Presunout tento kod jinam
    model.model.resize_token_embeddings(len(features_converter.tokenizer))

    train_dataset.features_fn = features_converter.convert_task_to_features
    test_dataset.features_fn = features_converter.convert_task_to_features

    framework = RelationExtractionFramework(model, save_model=args.save, **config)

    framework.train(train_dataset, test_dataset, config["model_dir"], 
                    config["output_file"])


if __name__ == "__main__":
    parser = parser_builder()
    args = parser.parse_args()
    if args.log:
        logging.config.fileConfig(args.log)
    else:
        logging.basicConfig(filename='remodel-train.log',level=logging.DEBUG if args.debug else logging.INFO)

    main(args)