#!/usr/bin/env python3
""" Rozhraní pro trénování a evaluaci modelů. 
"""
__author__ = "Karel Ondřej"


class BaseFramework(object):
    """ Rozhraní protrénování a evaluaci modelů.
    """
    def __init__(self, model, **kwargs):
        """ Inicializace.

        :param model: implementace modelu
        :type model: model.BaseModel
        """
        pass

    def train(self, train_dataset, test_dataset):
        """ Trénovaní modelu.

        :param train_dataset: trénovací datová sada
        :type train_dataset: torch.utils.data.dataset.Dataset
        :param test_dataset: validační datová sada
        :type test_dataset: torch.utils.data.dataset.Dataset
        """
        raise NotImplementedError
    
    def validate(self, test_dataset):
        """ Validování modelu.

        :param test_dataset: validační datová sada
        :type test_dataset: torch.utils.data.dataset.Dataset
        :returns: metriky
        :rtype: dict
        """
        raise NotImplementedError

    def predict(self, dataset):
        """ Predikce/klasifikace/... na datové sadě.

        :param dataset: vzorky k predikci
        :type dataset: torch.utils.data.dataset.Dataset
        """
        raise NotImplementedError

    def load_model(self, path):
        """ Načtení modelu ze souboru.
        :param path: cesta k souboru
        :type path: str
        """
        raise NotImplementedError

    def save_model(self, path):
        """ Uložení modelu do souboru.
        :param path: cesta k souboru
        :type path: str
        """
        raise NotImplementedError

    def __single_epoch_train(self, dataloader, optimizer, scheduler, criterion, 
                             epoch=0):
        """ Jedna epocha trénování, pomocná funkce.
        """
        raise NotImplementedError