__author__ = "Karel Ondřej"


import torch
from torch.utils.data.dataset import Dataset
from torch.utils.data import DataLoader, RandomSampler
import tqdm
import sklearn.metrics
import numpy as np
import sys
import os
import logging
import transformers
from transformers import BertConfig, BertModel, BertTokenizer

from .base import BaseFramework
from model import FeaturesConverter


class RelationExtractionFramework(BaseFramework):

    def __init__(self, model, batch_size=32, warmup_proportion=0.1, num_epoch=5,
                 learning_rate=2e-5, no_relation=None, save_model=False, 
                 save_validation=True, **kwargs):

        self.logger = logging.getLogger(self.__class__.__name__)

        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

        self.batch_size = batch_size
        self.warmup_proportion = warmup_proportion
        self.num_epoch = num_epoch
        self.learning_rate = learning_rate
        self.no_relation = no_relation
        self.save_checkpoint = save_model
        self.save_validation = save_validation
        self.model = model
        self.model.to(self.device)

    def train(self, train_dataset, test_dataset, model_dir, model_name):
        self.logger.info("Start training...")
        self.logger.info("Number of epochs: {}".format(self.num_epoch))
        self.logger.info("Batch size: {}".format(self.batch_size))
        self.logger.info("Learning rate: {}".format(self.learning_rate))

        if not os.path.isdir(model_dir):
            raise Exception("Wrong model directory...")

        import time
        ts = int(time.time())
        output_file = os.path.join(model_dir, "{}-{}".format(ts, model_name))

        train_sampler = RandomSampler(train_dataset)
        train_dataloader = DataLoader(
            train_dataset,
            sampler=train_sampler,
            batch_size=self.batch_size
        )

        num_training_steps = int(len(train_dataloader.dataset) / self.batch_size * self.num_epoch)
        num_warmup_steps = int(num_training_steps * self.warmup_proportion)

        param_optimizer = [(n, p) for n, p in self.model.named_parameters() if p.requires_grad]
        no_decay = ['bias', 'gamma', 'beta', 'LayerNorm.weight']     # from transformers library examples and https://mccormickml.com/2019/09/19/XLNet-fine-tuning/
        optimizer_grouped_parameters = [
            {'params': [p for n, p in param_optimizer if not any(nd in n for nd in no_decay)],
             'weight_decay_rate': 0.01},
            {'params': [p for n, p in param_optimizer if any(nd in n for nd in no_decay)],
             'weight_decay_rate': 0.0}
        ]

        optimizer = transformers.AdamW(
            optimizer_grouped_parameters, 
            lr=self.learning_rate, 
            correct_bias=False
        )
        scheduler = transformers.get_linear_schedule_with_warmup(
            optimizer, 
            num_warmup_steps=num_warmup_steps, 
            num_training_steps=num_training_steps
        )

        criterion = torch.nn.CrossEntropyLoss()

        for epoch in range(self.num_epoch):
            self.__single_epoch_train(train_dataloader, optimizer, scheduler, criterion, epoch)
            metrics = self.validate(test_dataset)

            self.logger.info("Precision: {:.4f}".format(metrics["precision"]))
            self.logger.info("Recall: {:.4f}".format(metrics["recall"]))
            self.logger.info("F1: {:.4f}".format(metrics["f1_score"]))

            if self.save_validation:
                predict_filename = "{}-F1-{:.6f}-P-{:.6f}-R-{:.6f}-epoch{}.tsv".format(
                    output_file, 
                    metrics["f1_score"],
                    metrics["precision"],
                    metrics["recall"],
                    epoch+1
                )
                with open(predict_filename, 'w') as f:
                    for idx, pred in enumerate(self.predict(test_dataset)):
                        line = "{}\t{}\n".format(idx, test_dataset.labels[pred["predicted_class"]])
                        f.write(line)


            if self.save_checkpoint:
                self.save_model("{}-F1-{:.6f}-P-{:.6f}-R-{:.6f}-epoch{}.pt".format(
                    output_file,
                    metrics["f1_score"],
                    metrics["precision"],
                    metrics["recall"],
                    epoch+1)
                )

    def __single_epoch_train(self, dataloader, optimizer, scheduler, criterion, epoch=0):
        self.model.train()

        sum_loss = 0

        for _, data in enumerate(dataloader):
            data = {key: data.to(self.device) for key, data in data.items()}
            logits = self.model(data)
            loss = criterion(logits, data["label_id"])
            
            optimizer.zero_grad()
            loss.backward()
            #torch.nn.utils.clip_grad_norm_(filter(lambda param: param.requires_grad, self.model.parameters()), 1.0)
            optimizer.step()
            scheduler.step()

            sum_loss += loss.item()

    @torch.no_grad()
    def validate(self, dataset):
        sampler = RandomSampler(dataset)
        dataloader = DataLoader(
            dataset,
            sampler=sampler,
            batch_size=self.batch_size
        )
        self.model.eval()

        no_relation_id = dataset.labels[self.no_relation] if self.no_relation else None

        total_preds = []
        total_labels = []

        for _, data in enumerate(dataloader):
            data = {key: data.to(self.device) for key, data in data.items()}
            logits = self.model(data)
            propabilities = torch.softmax(logits, -1)
            predicted_class = torch.argmax(propabilities, dim=1)
            
            total_preds += list(predicted_class.cpu().numpy())
            total_labels += list(data["label_id"].cpu().numpy())
        
        labels = np.unique(total_preds)

        if no_relation_id:
            labels = labels[labels != no_relation_id]

        precision = sklearn.metrics.precision_score(total_labels, total_preds, labels=labels, average="macro")
        recall = sklearn.metrics.recall_score(total_labels, total_preds, labels=labels, average="macro")
        f1_score = sklearn.metrics.f1_score(total_labels, total_preds, labels=labels, average="macro")

        return {
            "precision": precision,
            "recall": recall,
            "f1_score": f1_score
        }

    def predict(self, dataset):
        dataloader = DataLoader(
            dataset,
            batch_size=self.batch_size
        )
        self.model.eval()
        with torch.no_grad():
            for _, data in enumerate(dataloader):
                data = {key: data.to(self.device) for key, data in data.items()}
                logits = self.model(data)
                propabilities = torch.softmax(logits, -1).to(torch.device("cpu"))
                predicted_class = torch.argmax(propabilities, dim=1).to(torch.device("cpu"))
                
                for i in range(len(propabilities)):
                    yield {
                        "propabilities": propabilities[i],
                        "predicted_class": predicted_class[i]
                    }

    def save_model(self, path):
        self.model.to(torch.device("cpu"))
        torch.save(self.model, path)
        self.model.to(self.device)