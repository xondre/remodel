__author__ = "Karel Ondřej"

from framework.re import RelationExtractionFramework
from model.re import RelationExtractionModel
from dataloader.semeval10task8 import SemEval10Task8Dataset
from model.features import FeaturesConverter
import torch

import json
import sys
import logging
import logging.config


def parser_builder():
    import argparse
    parser = argparse.ArgumentParser(description='Predict relation from Sem Eval 2010 task 8 datatset.')
    parser.add_argument('--config', default="config.json")
    parser.add_argument('--model')
    parser.add_argument('--log', default='')
    return parser


def main(args):

    with open(args.config, "r") as f:
        config = json.load(f)

    test_dataset = SemEval10Task8Dataset(config["data_test"])
    
    config["label_list"] = test_dataset.labels
    config["labels_num"] = len(test_dataset.labels)
    config["tokenizer_name"] = config["model_path"]
    fetures_converter = FeaturesConverter(**config)

    test_dataset.features_fn = fetures_converter.convert_task_to_features
    
    model = torch.load(args.model)
    framework = RelationExtractionFramework(model, **config)

    for idx, prediction in enumerate(framework.predict(test_dataset)):
        predicted_class = model.labels[prediction["predicted_class"]]
        print("{}\t{}".format(idx, predicted_class))


if __name__ == "__main__":
    parser = parser_builder()
    args = parser.parse_args()
    if args.log:
        logging.config.fileConfig(args.log)
    else:
        logging.basicConfig(filename='remodel-predict.log',level=logging.DEBUG if args.debug else logging.INFO)

    main(args)