
__author__ = "Karel Ondřej"

from torch.utils.data.dataset import Dataset
from torch.utils.data import DataLoader, RandomSampler
import logging

LOGGER = logging.getLogger("train")


class SemEval10Task8Dataset(Dataset):

    def __init__(self, filename, features_fn=None):
        self.filename = filename
        self.labels = []
        self.tasks = self.read(filename)
        self.features_fn = features_fn

    def __len__(self):
        return len(self.tasks)

    def __getitem__(self, idx):
        features = self.tasks[idx]
        if self.features_fn:
            features = self.features_fn(features)

        return features

    def read(self, filename):
        output = []
        labels_set = set()
        with open(filename, "r") as f:
            while True:
                line = f.readline()
                LOGGER.debug('Sentence: ' + line)
                if line == '':
                    self.labels = sorted(tuple(labels_set))
                    return output
                task = {}
                cols = line2cols(line)
                task["sentence"] = cols[1]
                line = f.readline()
                LOGGER.debug('Label: ' + line)
                task["label"] = line.strip()
                labels_set.add(task["label"])
                line = f.readline()
                LOGGER.debug('Comment: ' + line)
                line = f.readline()
                output.append(task)

def line2cols(line):
    cols = line.strip().split('\t')
    return [cols[0], cols[1][1:-1]]
