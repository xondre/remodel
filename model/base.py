#!/usr/bin/env python3
""" Globální funkce, konstanty nebo proměnné pro model.
"""
__author__ = "Karel Ondřej"


import transformers


# podporované sítě typu transformers
MODEL_CLASSES = {
    "bert":    (transformers.BertConfig,    transformers.BertModel,    transformers.BertTokenizer),
    "albert":  (transformers.AlbertConfig,  transformers.AlbertModel,  transformers.AlbertTokenizer),
    "roberta": (transformers.RobertaConfig, transformers.RobertaModel, transformers.RobertaTokenizer),
    "xlnet":   (transformers.XLNetConfig,   transformers.XLNetModel,   transformers.XLNetTokenizer)
}
# index kontextové reprezentace
CLS_TOKEN_INDEX = {
    "bert":     0, 
    "albert":   0, 
    "roberta":  0,
    "xlnet":   -1
}


def sum_pool(tensor, sequence, indeces):
    """ Agragační funkce, která vytvoří průměr vektorů o velikost D z 
    podsekvence sekvence o délce N. Funkce pracuje s dávkou o velikosti B.

    :param tensor: místo v paměti pro uložení výsledku
    :type tensor: torch.Tensor[B][D]
    :param sequence: sekvenkce vektorů
    :type sequence: torch.Tensor[B][N][D]
    :param indeces: interval ze kterého se má udělat průměr
    :type indececs: torch.Tensor[B][2]
    :returns: referenci na argument tensor s uloženým výsledkem
    :rtype: torch.Tensor[B][D]
    """
    for batch_idx, entity_idx in enumerate(indeces):
        masked = sequence[batch_idx][entity_idx[0]+1:entity_idx[1]]
        if masked.size()[0] > 0:
            tensor[batch_idx] = masked.mean(axis=0)
    
    return tensor

def max_pool(tensor, sequence, indeces):
    """ Agragační funkce max-pooling, vracejíci vektor o velikost D z 
    podsekvence sekvence o délce N. Funkce pracuje s dávkou o velikosti B.

    :param tensor: místo v paměti pro uložení výsledku
    :type tensor: torch.Tensor[B][D]
    :param sequence: sekvenkce vektorů
    :type sequence: torch.Tensor[B][N][D]
    :param indeces: interval na kterém se provede max-pooling
    :type indececs: torch.Tensor[B][2]
    :returns: referenci na argument tensor s uloženým výsledkem
    :rtype: torch.Tensor[B][D]
    """
    for batch_idx, entity_idx in enumerate(indeces):
        masked = sequence[batch_idx][entity_idx[0]+1:entity_idx[1]]
        if masked.size()[0] > 0:
            tensor[batch_idx] = masked.max(0)[0]
    
    return tensor

def position_tokens_pool(tensor, sequence, indeces):
    """ Agragační funkce, která vytvoří repzentativní vektor o velikost D z 
    podsekvence sekvence o délce N sečtením prvního a posledního vektoru. 
    Funkce pracuje s dávkou o velikosti B.

    :param tensor: místo v paměti pro uložení výsledku
    :type tensor: torch.Tensor[B][D]
    :param sequence: sekvenkce vektorů
    :type sequence: torch.Tensor[B][N][D]
    :param indeces: interval na kterém se provede agregace
    :type indececs: torch.Tensor[B][2]
    :returns: referenci na argument tensor s uloženým výsledkem
    :rtype: torch.Tensor[B][D]
    """
    for batch_idx, entity_idx in enumerate(indeces):
        tensor[batch_idx] = sequence[batch_idx][entity_idx[0]] + sequence[batch_idx][entity_idx[1]]
    
    return tensor

def first_position_token_pool(tensor, sequence, indeces):
    """ Agragační funkce, která vytvoří repzentativní vektor o velikost D z 
    prvního vektoru podsekvence sekvence o velikosti N.
    Funkce pracuje s dávkou o velikosti B.

    :param tensor: místo v paměti pro uložení výsledku
    :type tensor: torch.Tensor[B][D]
    :param sequence: sekvenkce vektorů
    :type sequence: torch.Tensor[B][N][D]
    :param indeces: interval na kterém se provede agregace
    :type indececs: torch.Tensor[B][2]
    :returns: referenci na argument tensor s uloženým výsledkem
    :rtype: torch.Tensor[B][D]
    """
    for batch_idx, entity_idx in enumerate(indeces):
        tensor[batch_idx] = sequence[batch_idx][entity_idx[0]]
    
    return tensor

# agregační funkce
AGGREGATION_FUNCTIONS = {
    "max-pool": max_pool,
    "sum-pool": sum_pool,
    "position-tokens": position_tokens_pool,
    "first-position-token": first_position_token_pool,
}