#!/usr/bin/env python3
""" Model pro klasifikaci vztahů mezi pojmenovannými entitami založen na
předtrénované síti typu transformers a vytvoření reprezentace pro dvojici entit.
"""
__author__ = "Karel Ondřej"


import torch
import transformers
import logging

from .base import MODEL_CLASSES, AGGREGATION_FUNCTIONS, CLS_TOKEN_INDEX


class EntityPairLayer(torch.nn.Module):
    """ Vsrtva pro vytvoření reprezentace celkové reprezentace a reprezenbatce
    dvojice entit na zakladě operátorů:
    1. zřetězení entit
    2. rozdíl dvou entit
    3. element-wise product
    """
    def __init__(self, input_dim, hidden_dim, output_dim, concat=False,
                 difference=False, product=False, single_net=False,
                 activation=torch.nn.modules.activation.Tanh()):
        """ Inicializace.

        :param input_dim: dimenze vstupních vektorů
        :type input_dim: int
        :param hidden_dim: dimenze skryté vrstvy
        :type hidden_dim: int
        :param output_dim: dimenze výstupní vrstvy
        :type output_dim: int
        :param concat: povolit operátor zřetězení
        :type concat: bool
        :param difference: povolit operátor rozdílu
        :type difference: bool
        :param product: povolit operátor product-wise
        :type product: bool
        :param single_net: povolit operátor součtu
        :type single_net: bool
        :param activation: aktivační funkce
        :type activation: torch.nn.modules.Module
        """
        super(EntityPairLayer, self).__init__()

        self.concat = concat
        self.difference = difference
        self.product = product
        self.single_net = single_net

        self.tanh = torch.nn.modules.Tanh()

        dim = (1 + 2*concat + difference + product + single_net)*hidden_dim

        self.dropout = torch.nn.Dropout(0.1)

        self.dense0 = torch.nn.Sequential(
            activation,
            self.dropout,
            torch.nn.Linear(input_dim, hidden_dim)
        )

        self.dense1 = torch.nn.Sequential(
            self.dropout,
            torch.nn.Linear(input_dim, hidden_dim)
        )

        self.dense2 = torch.nn.Sequential(
            self.dropout,
            torch.nn.Linear(input_dim, hidden_dim)
        )

        self.transformation = torch.nn.Sequential(
            self.dropout,
            torch.nn.Linear(dim, output_dim),
            activation
        )

    def forward(self, cls, x, y):
        """ Vytvoření reprezentace pro klasifikaci.

        :param cls: reprezentace celé věty
        :type cls: torch.Tensor
        :param x: agregovaná reprezentace první entity
        :type x: torch.Tensor
        :param y: agregovaná reprezentace druhé entity
        :type y: torch.Tensor
        :returns: reprezentace pro klasifikaci vztahů
        :rtype: torch.Tensor
        """
        op = [cls]
        u = self.dense1(x)
        v = self.dense2(y)

        if self.concat:
            op.append(u)
            op.append(v)
        if self.difference:
            op.append(u-v)
        if self.product:
            op.append(u*v)
        if self.single_net:
            op.append(u+v)

        output = self.transformation(torch.cat(op, 1))

        return output


class EntityPairRelationExtractionModel(torch.nn.Module):
    """ Model pro klasifikaci vztahů mezi pojmenovanými entitami
    """
    def __init__(self, model_path, cache_dir, label_list=None, 
                 aggregation='sum-pool', concat=True, difference=True,
                 product=True, **kwargs):
        """ Inicializace.

        :param model_path: Cesta, nebo název, k předtrénovanému modelu. Název 
        modelu musí začínat názvem architektury sítě typu transformers.
        :type model_path: str
        :param cache_dir: Cesta k případnému stažení předtrénovaného modelu, atd.
        :type cache_dir: str
        :label_list: Seznam názvu tříd, do kterých se klasifikuje.
        :label_list: list
        :param aggregation: Agregační funkce pro vytvoření vektorové 
                            reprezentace entity.
        :type aggregation: str
        :param concat: Použít operátor konkatenace při vytvoření reprezenace 
                       dvojice entit.
        :type concat: bool
        :param difference: Použít operátor rozdílu při vytvoření reprezenace 
                           dvojice entit.
        :type difference: bool
        :param product: Použít operátor product-wise při vytvoření reprezenace 
                        dvojice entit.
        :type product: bool
        """
        super(EntityPairRelationExtractionModel, self).__init__()

        model_type = model_path.split("-")[0]

        self.labels = label_list
        num_labels = len(label_list) if label_list else 1

        self.aggregation = AGGREGATION_FUNCTIONS[aggregation]
        self.sentence_token = CLS_TOKEN_INDEX[model_type]
        config_class, model_class, _ = MODEL_CLASSES[model_type]

        model_config = config_class.from_pretrained(
            model_path, cache_dir=cache_dir
        )

        self.model = model_class.from_pretrained(
            model_path, config=model_config, cache_dir=cache_dir
        )

        self.dropout = torch.nn.Dropout(0.1)

        pair_dim = model_config.hidden_size

        self.pooled = torch.nn.Sequential(
            self.dropout,
            torch.nn.Linear(model_config.hidden_size, model_config.hidden_size),
            torch.nn.modules.Tanh()
        )

        self.entity_layer = EntityPairLayer(
            model_config.hidden_size, 
            model_config.hidden_size,
            pair_dim,
            concat=concat,
            difference=difference,
            product=product
        )

        self.output_layer = torch.nn.Sequential(
            self.dropout,
            torch.nn.Linear(
                pair_dim,
                num_labels
            ),
        )

    def forward(self, batch):
        """ Samotná implementace klasifikace.

        :param batch: dávka vstupních dat
        :type batch: dict
        :returns: pravděpodobnost příslušnosti vztahu do třídy
        """
        inputs = {
            "input_ids": batch["input_ids"],
            "attention_mask": batch["attention_mask"],
            "token_type_ids": batch["token_type_ids"]
        }
        outputs = self.model(**inputs)
        sequence_output = outputs[0]

        batch_size, _, hidden_size = sequence_output.shape

        y0_input = sequence_output[:, self.sentence_token]
        y1_input = torch.zeros(batch_size, hidden_size).to(self.device)
        y2_input = torch.zeros(batch_size, hidden_size).to(self.device)

        self.aggregation(y1_input, sequence_output, batch["entity1_ids"])
        self.aggregation(y2_input, sequence_output, batch["entity2_ids"])

        entity_rep = self.entity_layer(y0_input, y1_input, y2_input)

        logits = self.output_layer(entity_rep)

        return logits

    def to(self, *args, **kwargs):
        """ Přidělení paměti modelu zařízení (CPU vs. GPU).
        """
        self.device, _, _ = torch._C._nn._parse_to(*args, **kwargs)
        return super().to(*args, **kwargs)


class BiLinearEntityPairModel(torch.nn.Module):
    """ Model pro extrakci vztahů vytvářející reprezentaci dvojice entit pomocí
    BiLinear funkce (náročné na paměť, v práci nepoužito, možno dále prozkoumat).
    """
    def __init__(self, config):
        super(BiLinearEntityPairModel, self).__init__()
       
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.num_labels = config["labels_num"]
        aggregation_dict = {
            "max-pool": max_pool,
            "sum-pool": sum_pool,
            "position-token": first_position_token_pool
        }

        aggregation_fce = config["aggregation"] if "aggregation" in config and config["aggregation"] in aggregation_dict else "sum-pool"
        self.aggregation = aggregation_dict[aggregation_fce]

        self.sentence_token = {"bert": 0, "albert": 0, "xlnet": -1}[config["model_path"].split("-")[0]]
        config_class, model_class, _ = MODEL_CLASSES[config["model_path"].split("-")[0]]

        model_config = config_class.from_pretrained(
            config["model_path"], num_labels=self.num_labels, cache_dir=config["cache_dir"]
        )

        self.model = model_class.from_pretrained(
            config["model_path"], config=model_config, cache_dir=config["cache_dir"]
        )

        self.dropout = torch.nn.Dropout(0.1)

        self.dense1 = torch.nn.Sequential(
            self.dropout,
            torch.nn.Linear(model_config.hidden_size, model_config.hidden_size)
        )

        self.dense2 = torch.nn.Sequential(
            self.dropout,
            torch.nn.modules.Linear(model_config.hidden_size, model_config.hidden_size)
        )

        self.bilinear = torch.nn.Bilinear(
            model_config.hidden_size, 
            model_config.hidden_size,
            self.num_labels
        )

        self.output = torch.nn.Sequential(
            self.dropout,
            torch.nn.modules.Tanh(),
            torch.nn.Linear(self.num_labels, self.num_labels)
        ) 

    def forward(self, batch):
        inputs = {
            "input_ids": batch["input_ids"],
            "attention_mask": batch["attention_mask"],
            "token_type_ids": batch["token_type_ids"]
        }
        outputs = self.model(**inputs)
        sequence_output = outputs[0]

        batch_size, _, hidden_size = sequence_output.shape

        x = torch.zeros(batch_size, hidden_size).to(self.device)
        y = torch.zeros(batch_size, hidden_size).to(self.device)
        self.aggregation(x, sequence_output, batch["entity1_ids"])
        self.aggregation(y, sequence_output, batch["entity2_ids"])
        u = self.dense1(x)
        v = self.dense2(y)
        b = self.bilinear(self.dropout(x), self.dropout(y))

        logits = self.output(b + u + v)

        return logits