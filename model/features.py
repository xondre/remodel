#!/usr/bin/env python3
""" Předzpracování vstupu.
"""
__author__ = "Karel Ondřej"


import torch
import sys
import re
import logging

from .base import MODEL_CLASSES


class FeaturesConverter(object):
    """ Vytvoření rysů vstupní věty, které jsou vstupem pro model.
    """
    def __init__(self, tokenizer_name, cache_dir, label_list,
                 max_seq_length=128, entities_special_tokens=['<e1>', '</e1>', '<e2>', '</e2>'], 
                 **kwargs):
        """ Inicializace.

        :param tokenizer_name: název architektury předtrénované sítě
        :type tokenizer_name: str
        :param cache_dir: cesta pro případné stažení slovníku
        :type cache_dir: str
        :param max_seq_length: maximální délka vstupní věty
        :type max_seq_length: int
        :param entities_special_tokens: poziční značky ohraníčujíci entity
        :type entities_special_tokens: list
        """
        model_name = tokenizer_name.split("-")[0]

        _, _, tokenizer_class = MODEL_CLASSES[model_name]

        self.tokenizer = tokenizer_class.from_pretrained(
            tokenizer_name, cache_dir=cache_dir
        )
        
        self.label_list = label_list
        self.max_seq_len = max_seq_length

        self.entities_special_tokens = entities_special_tokens
        special_tokens = {
            "additional_special_tokens": self.entities_special_tokens
        }
        self.tokenizer.add_special_tokens(special_tokens)
        self.entities_special_ids = [self.tokenizer.convert_tokens_to_ids(token) for token in self.entities_special_tokens]

    def convert_task_to_features(self, task):
        """ Převedení vstupní věty.

        :param task: věta a pozice entit
        :type task: dict
        """
        logger = logging.getLogger(self.__class__.__name__)

        sentence = task["sentence"]
        sentence = re.sub(r"[ ]*<e1>[ ]*" , r" {}".format(self.entities_special_tokens[0]), sentence)
        sentence = re.sub(r"[ ]*</e1>[ ]*", r"{} ".format(self.entities_special_tokens[1]), sentence)
        sentence = re.sub(r"[ ]*<e2>[ ]*" , r" {}".format(self.entities_special_tokens[2]), sentence)
        sentence = re.sub(r"[ ]*</e2>[ ]*", r"{} ".format(self.entities_special_tokens[3]), sentence)

        tokens = self.tokenizer.tokenize(sentence)
        ids = self.tokenizer.convert_tokens_to_ids(tokens)
        
        logger.debug("Sentence: " + sentence)
        logger.debug("Tokens:   " + str(tokens))
        logger.debug("Ids:      " + str(ids))

        features = self.tokenizer.encode_plus(sentence,
            add_special_tokens=True,
            max_length=self.max_seq_len,
            pad_to_max_length=True
        )
        logger.debug("Encode:   " + str(features["input_ids"]))

        features["entity1_ids"] = [0, 0]
        features["entity2_ids"] = [0, 0]

        if len(features["input_ids"]) > self.max_seq_len:
            logger.warn("Sentence '{}' is longer that {} tokens (length={}).".format(sentence, self.max_seq_len, len(features["input_ids"])))

        try:
            features["entity1_ids"][0] = features["input_ids"].index(self.entities_special_ids[0])
        except ValueError:
            features["entity1_ids"][0] = self.max_seq_len-1
        try:
            features["entity1_ids"][1] = features["input_ids"].index(self.entities_special_ids[1], features["entity1_ids"][0]+1)
        except ValueError:
            features["entity1_ids"][1] = self.max_seq_len
        try:
            features["entity2_ids"][0] = features["input_ids"].index(self.entities_special_ids[2])
        except ValueError:
            features["entity2_ids"][0] = self.max_seq_len-1
        try:
            features["entity2_ids"][1] = features["input_ids"].index(self.entities_special_ids[3], features["entity2_ids"][0]+1)
        except ValueError:
            features["entity2_ids"][1] = self.max_seq_len

        if "label" in task:
            features["label_id"] = self.label_list.index(task["label"])

        features = {key: torch.tensor(value, dtype=torch.long) for key, value in features.items()}
        return features


class MaskedEntityFeaturesConvertor(object):
    """ Předpracování, které zamaskuje pojmenované entity. V práci nevyužito.
    """
    def __init__(self, config):

        model_name = config["model_path"].split("-")[0]
        cased = config["model_path"].split("-")[-1]

        _, _, tokenizer_class = MODEL_CLASSES[model_name]

        do_lower_case = {"cased": False, "uncased": True}.get(cased, config["do_lower_case"])

        self.tokenizer = tokenizer_class.from_pretrained(
            config["model_path"], do_lower_case=do_lower_case, cache_dir=config["cache_dir"]
        )

        self.max_seq_len = config["max_seq_length"]


    def convert_task_to_features(self, task, label_list):
        """
        """
        logger = logging.getLogger(self.__class__.__name__)

        sentence = task["sentence"]
        entity1 = re.search(r"<e1>(.*)</e1>", sentence).group(1).strip()
        entity2 = re.search(r"<e2>(.*)</e2>", sentence).group(1).strip()
        sentence = re.sub(r"[ ]*<e1>.*</e1>[ ]*", r" {} ".format(self.tokenizer.mask_token), sentence)
        sentence = re.sub(r"[ ]*<e2>.*</e2>[ ]*", r" {} ".format(self.tokenizer.mask_token), sentence)
        entity_pair = "{} {} {}".format(entity1, self.tokenizer.sep_token, entity2)

        tokens = self.tokenizer.tokenize(sentence)
        ids = self.tokenizer.convert_tokens_to_ids(tokens)
        
        logger.debug("Sentence: " + sentence)
        logger.debug("Tokens:   " + str(tokens))
        logger.debug("Ids:      " + str(ids))

        features = self.tokenizer.encode_plus(sentence,
            entity_pair,
            add_special_tokens=True,
            max_length=self.max_seq_len,
            pad_to_max_length=True
        )

        logger.debug("Encode:   " + str(self.tokenizer.convert_ids_to_tokens(features["input_ids"])))

        features["entity1_ids"] = [0, 0]
        features["entity2_ids"] = [0, 0]

        features["entity1_ids"][0] = features["input_ids"].index(self.tokenizer.mask_token_id)-1
        features["entity1_ids"][1] = features["entity1_ids"][0]+2
        features["entity2_ids"][0] = features["input_ids"].index(self.tokenizer.mask_token_id, features["entity1_ids"][1])-1
        features["entity2_ids"][1] = features["entity2_ids"][0]+2
        features["label_id"] = label_list.index(task["label"])

        features = {key: torch.tensor(value, dtype=torch.long) for key, value in features.items()}
        return features