#!/usr/bin/env python3
""" Reimplementace modelu R-BERT sloužící k experimentování.
Článek: https://arxiv.org/abs/1905.08284
"""
__author__ = "Karel Ondřej"


import transformers
import json
import sklearn.metrics
import numpy as np
import sys
import torch
from torch.utils.data.dataset import Dataset
from torch.utils.data import DataLoader, RandomSampler

from .base import MODEL_CLASSES, max_pool, sum_pool, first_position_token_pool


class RelationExtractionModel(torch.nn.Module):
    """ Model R-BERT sloužící k experimentování.
    Článek: https://arxiv.org/abs/1905.08284
    """
    def __init__(self, labels=None, batch_size=32,  **kwargs):
        super(RelationExtractionModel, self).__init__()
        self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
        self.labels = labels
        num_labels = len(labels) if labels else 1
 
        aggregation_dict = {
            "max-pool": max_pool,
            "sum-pool": sum_pool,
            "position-token": first_position_token_pool
        }

        self.aggregation = aggregation_dict[config["aggregation"]] if "aggregation" in config and config["aggregation"] in aggregation_dict else aggregation_dict["sum-pool"]

        config_class, model_class, _ = MODEL_CLASSES["bert"]

        model_config = config_class.from_pretrained(
            config["model_path"], num_labels=num_labels, cache_dir=config["cache_dir"]
        )
        
        self.model = model_class.from_pretrained(
            config["model_path"], config=model_config, cache_dir=config["cache_dir"]
        )

        self.hidden_size = model_config.hidden_size

        self.dropout = torch.nn.Dropout(0.1)

        self.sentence_layer = torch.nn.Sequential(
            self.dropout,
            torch.nn.modules.activation.Tanh(),
            torch.nn.Linear(model_config.hidden_size, model_config.hidden_size)
        )

        self.entity_layer = torch.nn.Sequential(
            self.dropout,
            torch.nn.modules.activation.Tanh(),
            torch.nn.Linear(model_config.hidden_size, model_config.hidden_size)
        )

        self.output_layer = torch.nn.Sequential(
            self.dropout,
            torch.nn.Linear(3*model_config.hidden_size, num_labels),
        )

    def forward(self, batch):
        inputs = {
            "input_ids": batch["input_ids"],
            "attention_mask": batch["input_mask"],
            "token_type_ids": batch["segment_ids"]
        }
        sequence_output, _ = self.model(**inputs)
        batch_size, _, hidden_size = sequence_output.shape

        y0_input = sequence_output[:,0,:]
        y1_input = torch.zeros(batch_size, hidden_size).to(self.device)
        y2_input = torch.zeros(batch_size, hidden_size).to(self.device)

        self.aggregation(y1_input, sequence_output, batch["entity1_ids"])
        self.aggregation(y2_input, sequence_output, batch["entity2_ids"])

        sentence = self.sentence_layer(y0_input)
        e1 = self.entity_layer(y1_input)
        e2 = self.entity_layer(y2_input)

        logits_input = torch.cat([sentence, e1, e2], 1)
        logits = self.output_layer(logits_input)
        return logits